//
//  ViewController.swift
//  HXSampleAPI
//
//  Created by Gaurav Keshre on 18/07/17.
//  Copyright © 2017 HXLOOP. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var lblResponse: UILabel!
        @IBOutlet weak var lblData: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "http://rest-service.guides.spring.io/greeting")!
        let req = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        req.httpMethod = "GET"
        let session = URLSession.shared
        
        
        let dataTask = session.dataTask(with: req as URLRequest) {data,response,error in
            let httpResponse = response as? HTTPURLResponse
            
            if (error != nil) {
                print(error!)
            } else {
                print(httpResponse!)
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
                    print("Result",result!)
                    DispatchQueue.main.async {
                        self.lblResponse.text = "\(String(describing: httpResponse!))"
                        self.lblData.text = "\(String(describing: result!))"
                    }
                } catch {
                    print("Error -> \(error)")
                }
            }
            
            
        }
        dataTask.resume()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

